require 'spec_helper'
require 'server'

describe FiltersCheck do
  let(:stats_name) { 'default' }
  let(:statistics) { [build(:statistics, name: stats_name, sample_max: sample_max)] }
  let(:filters_check) { FiltersCheck.new(statistics) }
  let(:treshold) { FiltersCheck.treshold(stats_name) }

  describe '#ok?' do
    subject { filters_check.ok? }

    context "filters overloaded" do
      let(:sample_max) { treshold + 100 }
      it { should be_false }
    end

    context "filters normal" do
      let(:sample_max) { treshold - 100 }
      it { should be_true }
    end
  end
end
