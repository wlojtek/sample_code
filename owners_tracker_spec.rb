require 'spec_helper'

describe OwnersTracker do
  let(:known_owner) { create(:owner) }
  let(:owners_tracker) { :OwnersTracker.new([known_owner.owner_id]) }
  let(:response_body) { create(:owner_response) }
  let(:new_owner) { create(:new_owner)}
  
  describe "#launch" do
    subject { owners_tracker.launch }

    context "don't have owner " do
      let(:known_owner) { nil }
      it "increase queue size" do
        expect { subject }.to change(OwnersTrackerWorker.jobs, :size).by(1)
      end
    end

    context "have owner" do
      let(:known_owner) { create(:owner) }
      it "don't increase queue size" do
        expect { subject }.to_not change(OwnersTrackerWorker.jobs, :size).by(0)
      end
    end
  end

  describe "#get_owners_for_companies" do
    subject { owners_tracker.get_owners_for_companies }

    before do
      owners_tracker.stub(:fetch_owners_details).and_return(response_body)
    end

    it "update owner" do
      subject
      owner.should eq(new_owner)
    end
  end
end
