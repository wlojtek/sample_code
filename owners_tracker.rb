class OwnersTracker
  OWNERS_LIMIT = 50

  attr_reader :owner_ids

  def initialize(company_ids)
    @company_ids = company_ids
  end

  def launch
    get_company_ids.each do |ids|
      OwnersTrackerWorker.perform_async(ids) if ids.any?
    end
  end

  def get_owners_for_companies
    owners = fetch_owners
    owners.each do |owner_data|
      begin
        company_id = owner.company_id
        owner = get_owner(company_id)
        update_owner(owner, owner_data)
      rescue => e
        ExternalMonitor.notify_or_raise(e)
      end
    end
  end

  def owner_ids
    @owner_ids.uniq
  end

  private

  def get_company_ids
    @company_ids.in_groups_of(OWNERS_LIMIT, false)
  end

  def fetch_owners
    Companies.list(prepare_company_ids)
  end

  def prepare_company_ids
    @company_ids.join(',')
  end

  def get_owner(company_id)
    Owner.where(company_id: company_id).first_or_create
  end

  def update_owner(owner, owner_data)
    params = {
      type: owner_data['type'],
      title: owner_data['title']
    }
    owner.update_attributes!(params) if asset
  end
end
