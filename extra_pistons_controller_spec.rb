require 'spec_helper'

describe ExtraPistonsController do
  let(:car) { Car.find_by_type('good') }
  let(:user) { Factory.create(:user, car: car) }
  let(:initial_path) { pistons_path }

  describe '#new' do
    before do
      controller.stub(:current_user => user)
      @request.env['HTTP_REFERER'] = initial_path
      xhr :get, :new
    end

    context 'logged in' do
      it { response.should be_success }

      context "user has good card" do
        it 'return extra pistons options' do
          response.body.should ==  { :options => ExtraPiston::OPTIONS }.to_json
        end
      end

      context "user doesn't have good car" do
        let!(:car) { Car.bad }

        it { response.status.should == 403 }
      end
    end

    context 'logged out' do
      let(:user) { nil }

      it { response.should redirect_to(login_path) }
    end
  end

  describe '#create' do
    context 'logged in' do
      let(:piston_params) { { :extra_piston => { :amount => 1, :hours => 1 } } }

      before(:each) do
        controller.stub :current_user => user
        @request.env['HTTP_REFERER'] = initial_path
        ExtraPiston.stub(:launch).and_return(status)
        post :create, pistons_params
      end

      context "launch successful" do
        let(:status) { true }

        it { response.should redirect_to(initial_path) }
        it { flash[:success].should be_present }
      end

      context "launch successful" do
        let(:status) { false }

        it { response.should redirect_to(initial_path) }
        it { flash[:error].should be_present }
      end
    end

    context 'logged out' do
      before do
        controller.stub :current_user => nil
        post :create, :extra_piston => { :amount => 1, :hours => 1 }
      end

      it { response.should redirect_to(login_path) }
    end
  end
end
