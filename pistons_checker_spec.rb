require 'spec_helper'

describe PistonsChecker do
  let(:user) { Factory.create(:user, extra_pistons: extra_pistons) }
  let(:extra_pistons) { 20 }

  let!(:extra_piston) { Factory.create(:extra_piston, user: user) }

  describe '#perform' do
    before do
      ExtraPiston.stub(:running).and_return([extra_piston])
      extra_piston.stub(:expired?).and_return(expired)
      PistonsChecker.new.perform
    end

    context "extra piston expired" do
      let(:expired) { true }

      it 'changed piston status to expired' do
        extra_pistons.status.should == ExtraPiston::EXPIRED
      end

      it 'user extra pistons reduced' do
        user.extra_pistons.should == 1
      end
    end

    context "extra piston not expired" do
      let(:expired) { false }

      it 'piston status is still running' do
        extra_pistons.status.should == ExtraPiston::RUNNING
      end

      it 'user extra pistons should not be reduced' do
        user.extra_pistons.should == extra_pistons
      end
    end
  end
end
