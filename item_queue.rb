module Fake
  class ItemQueue < ActiveRecord::Base

    INIT         = 'initialize'
    IN_PROCESS   = 'in_process'
    POLLED       = 'polled'
    ERROR        = 'error'

    attr_accessible :user_id, :property_id, :type, :status

    enum type: [:type_1, :type_2, :type_3]
    enum status: [ INIT, IN_PROCESS, ERROR ]

    after_initialize :set_default_status

    scope :preprocessing, -> { where(status: [INIT, IN_PROCESS]) }
    scope :by_user, -> (user_id) { where(user_id: user_id) }

    belongs_to :block
    belongs_to :user

    validates :block, presence: true
    validates :property, presence: true

    def self.poll(content_id)
      item = preprocessing.first
      item.update_attributes!(status: POLLED) if item
      item
    end

    def mark_as_error!
      update_attributes!(status: ERROR)
    end

    private

      def set_default_status
        self.status ||= INIT
      end
  end
end
