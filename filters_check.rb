class FiltersCheck < SystemCheck
    DEFAULT_TRESHOLD = 1_000
    TRESHOLDS = {
      :acm_filter => 15_000
    }

    attr_accessor :filters_overloaded

    def initialize(statistics)
      @filters_overloaded = statistic.select { |s| self.filter_overloaded?(s) }
    end

    def ok?
      @filters_overloaded.empty?
    end

    def message
      "Problems with filters : #{@filters_overloaded}"
    end

    def self.treshold(name)
      TRESHOLDS[name.to_sym] || DEFAULT_TRESHOLD
    end

    def filter_overloaded?(stats)
      stats.sample_max > self.class.treshold(stats.name)
    end
end
