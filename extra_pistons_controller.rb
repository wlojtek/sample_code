class ExtraPistonsController < SecureController
  before_filter :restrict_to_users_with_good_car

  def new
    respond_to do |format|
      format.json { render :json => { options: ExtraPistons::OPTIONS } }
    end
  end

  def create
    amount = ExtraPistons.getAmount(params[:amount])
    hours = ExtraPistons.getHours(params[:hours])

    piston = ExtraPiston.build(amount: amount, hours: hours)
	  
    if piston.launch
      flash[:success] = t(:launch_extra_pistons_success)
    else
      flash[:error] = t(:launch_extra_pistons_error)
    end

    redirect_to :back
  end

  def restrict_to_users_with_good_car
    return if car.good?

    respond_to do |format|
      format.html do
        flash[:error] = msg
        redirect_to :back
      end

      format.js do
        render :partial => 'pistons/upgrade_info', :status => :forbidden
      end
    end
  end
end
